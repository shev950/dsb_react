import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import {initReactI18next} from "react-i18next";

i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources: {
            ua: {
                translations: {
                    "welcome": "Ласкаво просимо",
                    "activation service": "на сервіс активації доповнень",
                    for_train_simulator: "для Train Simulator",
                    download_key: "Завантажте Ваш ключ установки доповнення<1/> для його активації.",
                    "select file": "Вибрати файл",
                    "ask question": "Задати питання",
                    "have questions": "Є питання?",
                    write_to_mail: "Напишіть нам на <1>divovigna@gmail.com</1> і ми відповімо Вам як можна швидше",
                    "home": "На головну",
                    "file done": "Ваш ключ активації готовий",
                    "place the key": "Додайте отриманий ключ активації поруч з Вашим раніше відправленим ключем установки.",
                    download_did_not_start: "Якщо скачування не розпочалося, натисніть <1 href=\"href={props.activationKeyLink} download={props.activationKeyFile}\">тут</1>.",
                    "paid add-on": "Платне доповнення",
                    write_to_mail_detail: "Напишіть нам на <1>divovigna@gmail.com</1> для отримання ключа.",
                    try_again_or_write_to_mail_detail: "Спробуйте ще раз або напишіть нам на <1>divovigna@gmail.com</1> для отримання ключа.",
                    recaptcha_info: "Цей сайт захищений reCAPTCHA і застосовується <1>Політика конфіденційності</1> та <2>Умови обслуговування</2> Google.",
                    have_a_nice_game: "Приємної гри!",
                    "product_not_found": "Продукт не знайдено",
                    "request_rejected": "Запит відхилено",
                    "validation_error": "Неправильний ключ",
                }
            },
            ru: {
                translations: {
                    "welcome": "Добро пожаловать",
                    "activation service": "на сервис активации дополнений",
                    for_train_simulator: "для Train Simulator",
                    download_key: "Загрузите Ваш ключ установки дополнения<1/> для его активации.",
                    "select file": "Выбрать файл",
                    "ask question": "Задать вопрос",
                    "have questions": "Есть вопросы?",
                    write_to_mail: "Напишите нам на <1>divovigna@gmail.com</1> и мы ответим Вам как можно скорее",
                    "home": "На главную",
                    "file done": "Ваш ключ активации готов",
                    "place the key": "Разместите полученный ключ активации рядом с Вашим ранее отправленным ключом установки.",
                    download_did_not_start: "Если скачивание не началось, нажмите <1 href=\"href={props.activationKeyLink} download={props.activationKeyFile}\">здесь</1>.",
                    "paid add-on": "Платное дополнение",
                    write_to_mail_detail: "Напишите нам на <1>divovigna@gmail.com</1> для получения ключа.",
                    try_again_or_write_to_mail_detail: "Попробуйте еще раз или напишите нам на <1>divovigna@gmail.com</1> для получения ключа.",
                    recaptcha_info: "Этот сайт защищен reCAPTCHA и применяются <1>Политика конфиденциальности</1> и <2>Условия обслуживания</2> Google.",
                    have_a_nice_game: "Приятной игры!",
                    "product_not_found": "Продукт не найден",
                    "request_rejected": "Запрос отклонен",
                    "validation_error": "Неверный ключ",
                }
            },
            en: {
                translations: {
                    "welcome": "Welcome",
                    "activation service": "to",
                    for_train_simulator: "add-ons activation service <1/> for Train Simulator",
                    download_key: "Upload your add-on installation key<1/> to activate it.",
                    "select file": "Select file",
                    "ask question": "Ask Question",
                    "have questions": "Have questions?",
                    write_to_mail: "Contact us via <1>divovigna@gmail.com</1> and we will respond you as soon as possible",
                    "home": "To the main",
                    "file done": "Your activation key is ready",
                    "place the key": "Place the received activation key next to your previously sent installation key.",
                    download_did_not_start: "If the download did not start, click <1 href=\"href={props.activationKeyLink} download={props.activationKeyFile}\">here</1>.",
                    "paid add-on": "Paid add-on",
                    write_to_mail_detail: "Contact us via <1>divovigna@gmail.com</1> to get the key.",
                    try_again_or_write_to_mail_detail: "Try again or contact us via <1>divovigna@gmail.com</1> to get the key.",
                    recaptcha_info: "This site is protected by reCAPTCHA and the Google <1>Privacy Policy</1> and <2>Terms of Service</2> apply.",
                    have_a_nice_game: "Have a nice game!",
                    "product_not_found": "Product not found",
                    "request_rejected": "Request rejected",
                    "validation_error": "Invalid key",
                }
            }
        },
        fallbackLng: "ru",
        debug: true,

        ns: ["translations"],
        defaultNS: "translations",

        keySeparator: false,

        interpolation: {
            escapeValue: false
        }
    });

export default i18n;