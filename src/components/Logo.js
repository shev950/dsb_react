import React, {Component} from "react";

class Logo extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="form__logo">
                    <div className="logo__animation"/>
                    <img src={require('../images/DSB_logo.svg')} alt="logo"/>
                </div>
            </React.Fragment>
        );
    }
}

export default Logo;