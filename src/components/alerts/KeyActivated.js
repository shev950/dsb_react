import {Trans, useTranslation} from "react-i18next";
import React, {Component} from "react";
import MainPage from "../pages/MainPage";

class KeyActivated extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMainPage: false,
        }
    }

    render() {
        if (this.state.showMainPage) {
            return <MainPage/>
        }
        return <Content
            okButtonClickHandler={this.showMainPage}
            activationKeyLink={this.props.activationKeyLink}
            activationKeyFile={this.props.activationKeyFile}
        />
    }

    showMainPage = () => {
        this.setState({
            showMainPage: true,
        });
    };
}

const Content = props => {
    const {t} = useTranslation();
    return (
        <React.Fragment>
            <h1>{t("file done")}</h1>
            <p>{t("place the key")}</p>
            <p><Trans i18nKey="download_did_not_start"> <a
                href={props.activationKeyLink}
                download={props.activationKeyFile}
            > </a> </Trans></p>
            <span className="span__weight">{t("have_a_nice_game")}</span>
            <button className="form__button" onClick={props.okButtonClickHandler}>Ок</button>
        </React.Fragment>
    );
};

export default KeyActivated;