import {Trans, useTranslation} from "react-i18next";
import React, {Component} from "react";
import MainPage from "../pages/MainPage";

class PaymentRequired extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMainPage: false,
        }
    }

    render() {
        if (this.state.showMainPage) {
            return <MainPage/>
        }
        return <Content
            okButtonClickHandler={this.showMainPage}
        />
    }

    showMainPage = () => {
        this.setState({
            showMainPage: true,
        });
    };
}

const Content = props => {
    const {t} = useTranslation();
    return (
        <React.Fragment>
            <h1>{t("paid add-on")}</h1>
            <p><Trans i18nKey="write_to_mail_detail"> <a href="mailto:divovigna@gmail.com">divovigna@gmail.com</a> </Trans></p>
            <button className="form__button" onClick={props.okButtonClickHandler}>Ок</button>
        </React.Fragment>
    );
};

export default PaymentRequired;