import React, {Component} from "react";
import {useTranslation, Trans} from "react-i18next";
import KeyActivated from "../alerts/KeyActivated";
import PaymentRequired from "../alerts/PaymentRequired";
import ProductNotFound from "../alerts/ProductNotFound";
import RequestRejected from "../alerts/RequestRejected";
import InvalidKey from "../alerts/InvalidKey";
import { ReCaptcha } from 'react-recaptcha-v3';

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.submitFormRef = React.createRef();
        this.state = {
            showKeyActivatedAlert: false,
            showPaymentRequiredAlert: false,
            showProductNotFoundAlert: false,
            showRequestRejectedAlert: false,
            showInvalidKeyAlert: false,
            activationProcessing: false,
            activationKeyLink: null,
            activationKeyFile: null,
            recaptchaToken: null
        }
    }

    verifyCallback = (recaptchaToken) => {
        this.state.recaptchaToken = recaptchaToken;
    }

    render() {
        if (this.state.showKeyActivatedAlert === true) {
            return <KeyActivated
                activationKeyLink={this.state.activationKeyLink}
                activationKeyFile={this.state.activationKeyFile}
            />
        }
        if (this.state.showPaymentRequiredAlert === true) {
            return <PaymentRequired/>;
        }
        if (this.state.showProductNotFoundAlert === true) {
            return <ProductNotFound/>;
        }
        if (this.state.showRequestRejectedAlert === true) {
            return <RequestRejected/>;
        }
        if (this.state.showInvalidKeyAlert === true) {
            return <InvalidKey/>;
        }
        return <Content
            fileChangeHandler={this.handleFileChange}
            formSubmitHandler={this.handleFormSubmit}
            submitFormRef={this.submitFormRef}
            verifyCallback={this.verifyCallback}
            recaptchaToken={this.state.recaptchaToken}
            activationProcessing={this.state.activationProcessing}
        />
    }

    /**
     * Обработчик события, когда в поле выбран файл
     * @param event
     */
    handleFileChange = (event) => {
        this.submitFormRef.current.click();
    };

    /**
     * Обработчик события, когда форма идет на отправку
     * @param event
     */
    handleFormSubmit  = (event) => {
        // Включаем аницмацию обработки запроса
        // this.state.activationProcessing = true;
        this.setState({
            activationProcessing: true
        });
        // Отмена события по умолчанию, в этом случае - отмена отправки
        // формы браузером, потому что мы отправляем ее вручную асинхронно
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.append('recaptcha_token', this.state.recaptchaToken);
        // Отправка данных формы асинхронно
        this.postData('/api/activate', formData)
            .then((response) => {
                // TODO Сделать обработку ошибок валидации
                if (response.status === 402) {
                    // Дополнение нуждается в оплате. Инициируем вывод алерта, что нужна оплата
                    this.setState({
                        showPaymentRequiredAlert: true
                    });
                }
                if (response.status === 403) {
                    // Запрос отклонен (например, отклонено капчей)
                    this.setState({
                        showRequestRejectedAlert: true
                    });
                }
                if (response.status === 404) {
                    // Дополнение не найдено
                    this.setState({
                        showProductNotFoundAlert: true
                    });
                }
                if (response.status === 422) {
                    // Запрос или ключ не валидный
                    this.setState({
                        showInvalidKeyAlert: true
                    });
                }
                // Если ответ успешный
                if (response.ok === true) {
                    // Делаем Blob из данных файла, который содержится в ответе
                    return response.blob()
                        .then((data) => {
                            /*
                             * Извлечение имени файла из заголовка Content-Disposition
                             */
                            let filename = "";
                            // Имя файла содержится в заголовке Content-Disposition
                            let disposition = response.headers.get('Content-Disposition');
                            if (disposition) {
                                let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                                let matches = filenameRegex.exec(disposition);
                                if (matches != null && matches[1]) {
                                    filename = matches[1].replace(/['"]/g, '');
                                }
                            }

                            /**
                             * Создаем Blob из полученного файла и через временную ссылку отдаем его на скачивание
                             */
                            let fileUrl = window.URL.createObjectURL(data);
                            let tempLink = document.createElement('a');
                            tempLink.href = fileUrl;
                            this.setState({
                                showKeyActivatedAlert: true,
                                activationKeyLink: fileUrl,
                                activationKeyFile: filename,
                                activationProcessing: false
                            });
                            tempLink.setAttribute('download', filename);
                            tempLink.click();
                        }).catch((err) => {
                            console.error(err);
                        })
                }
            })
            .catch(error => console.error(error));
    };

    /**
     * Метод настройки отправки данных асинхронным запросом
     * @param url
     * @param data
     * @returns {Promise<Response>}
     */
    postData(url = '', data = {}) {
        return fetch(url, {
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Accept': 'text/plain',
            },
            body: data,
        })
        .then(response => response);
    }
}

const Content = props => {
    const {t} = useTranslation();
    return (
        <React.Fragment>
            <ReCaptcha
                sitekey="6LdJsfoUAAAAAIysgI5Cs3HuO8gOwoqEenUyme_E"
                action='activate'
                verifyCallback={props.verifyCallback}
            />
            <h1>{t("welcome")}</h1>
            <p>{t("activation service")}<br/>
                <span>DSBTeam</span><br/>
                <Trans i18nKey="for_train_simulator"> <br/> </Trans></p>
            <strong><Trans i18nKey="download_key"> <br/> </Trans></strong>
            <form onSubmit={props.formSubmitHandler} >
                <label className="form__button">
                    {props.activationProcessing === false &&
                        t("select file")
                    }
                    {props.activationProcessing === true &&
                        <i className="fas fa-cog fa-spin"> </i>
                    }
                    <input
                        type="file"
                        name="key"
                        className="form__button-file"
                        onChange={props.fileChangeHandler}
                        disabled={props.activationProcessing}
                    />
                </label>
                <input type="submit" ref={props.submitFormRef} style={{display: "none"}}/>
            </form>
            <p className={"recaptcha-info"}>
                <Trans i18nKey="recaptcha_info"> <a href="https://policies.google.com/privacy">Privacy Policy</a>
                    <a href="https://policies.google.com/terms">Terms of Service</a></Trans>
            </p>
        </React.Fragment>
    );
};

export default MainPage;