import React from "react";
import {NavLink} from "react-router-dom";
import {useTranslation, Trans} from "react-i18next";

export default function MainPage() {
    const {t} = useTranslation();

    return (
        <React.Fragment>
            <h1>{t("have questions")}</h1>
            <p><Trans i18nKey="write_to_mail"> <a href="mailto:divovigna@gmail.com">divovigna@gmail.com</a> </Trans></p>
            <NavLink className="form__button" to="/">{t("home")}</NavLink>
        </React.Fragment>
    );
}