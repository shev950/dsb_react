import React, {Component} from "react";
import {
    Route,
    Redirect,
    Switch
} from "react-router-dom";
import Logo from "./Logo";
import MainPage from "./pages/MainPage";
import AskQuestion from "./pages/AskQuestion";

class Form extends Component {
    render() {
        return (
            <div className="content">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="form">
                                <Logo/>
                                <Switch>
                                    <Route exact path="/" component={MainPage}/>
                                    <Route path="/ask-question" component={AskQuestion}/>
                                    <Route path="*">
                                        <Redirect to="/"/>
                                    </Route>
                                </Switch>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Form;