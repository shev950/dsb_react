import React from "react";
import {NavLink} from "react-router-dom";
import {useTranslation} from "react-i18next";

export default function Footer() {
    const {t, i18n} = useTranslation();

    const changeLanguage = (lng, event) => {
        i18n.changeLanguage(lng);
        event.preventDefault();
    };

    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="footer">
                            <div className="footer__email">
                                <i className="fas fa-envelope"/>
                                <NavLink to="/ask-question">{t("ask question")}</NavLink>
                            </div>
                            <div className="footer__language">
                                <i className="fas fa-globe"/>
                                {/*eslint-disable-next-line jsx-a11y/anchor-is-valid*/}
                                <a href="#" onClick={(event) => changeLanguage("en", event)}>English</a>
                                {/*eslint-disable-next-line jsx-a11y/anchor-is-valid*/}
                                <a href="#" onClick={(event) => changeLanguage("ru", event)}>Русский</a>
                                {/*eslint-disable-next-line jsx-a11y/anchor-is-valid*/}
                                <a href="#" onClick={(event) => changeLanguage("ua", event)}>Українська</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
}