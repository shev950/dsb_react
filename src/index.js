import React from 'react';
import ReactDOM from 'react-dom';
import './reset.css';
import './fonts/font-awesome/css/all.min.css';
import './bootstrap-grid.min.css';
import "./i18n";
import './index.css';
import Main from './Main';

ReactDOM.render(
    <Main />,
    document.getElementById('root')
);