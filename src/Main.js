import React, {Component} from "react";
import Form from "./components/Form";
import Footer from "./components/Footer";
import {HashRouter} from "react-router-dom";
import {loadReCaptcha} from "react-recaptcha-v3";

class Main extends Component {
    render() {
        return (
            <div className={"background"}>
                <HashRouter>
                    <Form/>
                    <Footer/>
                </HashRouter>
            </div>
        );
    }

    componentDidMount() {
        loadReCaptcha('6LdJsfoUAAAAAIysgI5Cs3HuO8gOwoqEenUyme_E');
    }
}

export default Main;